import numpy as np

class Neuron(object):

    def __init__(self, input_num, parent):
        self.input_num = input_num
        self.parent = parent #array parents
        self.weights = np.random.rand(input_num)
        self.gradients = np.random.rand(input_num)
        self.output = 0
        self.delta = 0


    def activation_func(self, sum):
        return 1 / (1 + np.exp(-sum))

    def run(self, inputs):
        self.output = np.matmul(inputs, self.weights)
        return np.matmul(inputs, self.weights)

    def delta_calculation(self, error):
        
        self.delta = error * self.output * (1 - self.output)
        self.weights

    def update_weights(self, error):
        for weight in self.weights:
            weight -= alpha*self.gradi(error)
