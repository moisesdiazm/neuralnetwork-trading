import pandas as pd
import numpy as np
import math as m
import datetime


def preprocessing_csv():
    print("Reading CSV")
    df = pd.read_csv('validationSet.csv')
    df.to_pickle("preprocessed_data")
    return df


def preprocess_samples():
    # Preprocess samples
    df = None
    try:
        df = pd.read_pickle("preprocessed_data")
        print("Preprocessed Data Found")
    except:
        print("Preprocessed file doesn't exist. Creating file.")
        df = preprocessing_csv()

    return df


