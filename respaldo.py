# Neural network implementation
#

import numpy as np
from matplotlib import pyplot as plt

def test_function(x):
    return 0.5*(x**3) - 0.3*(x**2) + 10

x_samples = np.array([[x] for x in np.linspace(-10, 10, 10)])
y_samples = np.array([y[0] for y in test_function(x_samples)])

plt.plot(x_samples, y_samples)
plt.show()
#
# x_samples = np.array([
#     [2],
#     [4],
#     []
# ])
#
# y_samples = np.array([10.0, 30])

NEURONS_QTY = 3
X_VECTOR_LENGTH = len(x_samples[0])
ETA = 0.0001

w_matrix = np.random.rand(NEURONS_QTY, X_VECTOR_LENGTH + 1) # +1 for bias
j_vector = np.random.rand(NEURONS_QTY)
a_vector = np.zeros(NEURONS_QTY)

#Deltas matrix. For hidden layer z
z_vector = np.zeros(NEURONS_QTY)
#For output layer m
m_vector = np.zeros(1)

#Gradients matrix. For input weights wg
wg_vector = np.zeros(X_VECTOR_LENGTH + 1)
#Gradients for output weights
og_vector = np.zeros(NEURONS_QTY)


# w_matrix = np.array([[0.94442221, 0.60891922, 0.8681712, 0.62541323], [0.96158112,0.61785532,0.94325846,0.16133993], [0.766387,0.71727074,0.57041841,0.28283347]])
# j_vector = np.array([1,2,3])

def activation(summation):
    return 1/(1 + np.exp(-summation))

def feed_forward(sample, output_sample):
    global a_vector
    # Hidden neurons output considering bias
    a_vector = activation(np.matmul(w_matrix, np.insert(sample, 0, 1)))
    # Output neuron output
    output = activation(np.matmul(j_vector,a_vector))
    #Error calculation
    error =  output*1400 - 700 - output_sample
    sqerror = (error)**2

    return sqerror, error, output


def generate_deltas(error, output):
    global m_vector
    global z_vector
    m_vector[0] = error*output*(1-output)
    # weighted deltas for hidden layer b vector
    for i in range(NEURONS_QTY):
        b_vector = m_vector[0]*j_vector[i]
        z_vector[i] = b_vector*a_vector[i]*(1-a_vector[i])

def generate_gradients(sample):
    global w_matrix, m_vector, a_vector, z_vector
    sample = np.insert(sample, 0, 1)
    for i in range(NEURONS_QTY):
        j_vector[i] -= a_vector[i] * m_vector[0] * ETA

    for i in range(NEURONS_QTY):
        for j in range(X_VECTOR_LENGTH + 1):
            w_matrix[i][j] -= z_vector[i]*sample[j]*ETA

def backpropagation(sample, error, output):
    generate_deltas(error, output)
    generate_gradients(sample)


if __name__ == "__main__":

    sum_err = 10000
    while sum_err > 0.000001:
        sum_err = 0
        for i in range(len(y_samples)):
            sqerror, error, output = feed_forward(x_samples[i], y_samples[i])
            # print("////// {} : {} //////////".format(output, y_samples[i]))
            sum_err += sqerror
            backpropagation(x_samples[i], error, output)
        print("\r Error progress: {}".format(sum_err), end="")

    print("///////////////////////////////////// \n \nFinal weights matrix: \n")
    print(w_matrix)
    print("\n \nOutput weights matrix: \n")
    print(z_vector)
    print(output)


    # Generate evidence
    y_tested = []
    for x in x_samples:
        _, _, output = feed_forward(x, 0)
        y_tested.append(output)

    plt.plot(x_samples, y_samples, x_samples, y_tested)
    plt.show()

    np.save("w_matrix", w_matrix)
    np.save("j_vector", j_vector)