import pandas as pd
import numpy as np
import math as m
import datetime
from matplotlib import pyplot as plt

scaling_factors = []
scaled_samples = []
channel_num = 0

def normalize_input(x_samples):
    global channel_num
    channel_num = len(x_samples[0])

    for i in range(channel_num):
        channel_mean = np.mean([x[i] for x in x_samples])
        channel_std_dev = np.std([x[i] for x in x_samples])
        print("Channel {} > mean: {} std: {}".format(i, channel_mean, channel_std_dev))
        scaling_factors.append({'mean': channel_mean, 'std': channel_std_dev})

    for sample in x_samples:
        scaled_sample = []
        for channel in range(channel_num):
            new_value = (sample[channel] - scaling_factors[channel]['mean']) / scaling_factors[channel]['std']
            scaled_sample.append(new_value)
        scaled_samples.append(scaled_sample)

    return scaled_samples, scaling_factors


def normalize_single_input(sample):
    scaled_sample = []
    for channel in range(channel_num):
        new_value = (sample[channel] - scaling_factors[channel]['mean']) / scaling_factors[channel]['std']
        scaled_sample.append(new_value)
    return scaled_sample