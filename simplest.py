# Neural network implementation
#

import numpy as np
from matplotlib import pyplot as plt


x_samples = np.array([
    [0.05, 0.1]
])

y_samples = np.array([
    [0.01, 0.99]
])

NEURONS_QTY = 2
OUTPUT_QTY = 2
X_VECTOR_LENGTH = len(x_samples[0])
ETA = 0.5

w_matrix = np.random.rand(NEURONS_QTY, X_VECTOR_LENGTH + 1) # +1 for bias
j_matrix = np.random.rand(OUTPUT_QTY, NEURONS_QTY)
a_vector = np.zeros(NEURONS_QTY) #output from hidden layer

#Deltas matrix. For hidden layer z
z_vector = np.zeros(NEURONS_QTY)
#For output layer m
m_vector = np.zeros(len(y_samples[0]))

#Gradients matrix. For input weights wg
wg_vector = np.zeros(X_VECTOR_LENGTH + 1)
#Gradients for output weights
og_vector = np.zeros(NEURONS_QTY)


# w_matrix = np.array([[0.35, 0.15, 0.20], [0.35,0.25,0.30]]) #WITH BIAS
# j_matrix = np.array([[0.4,0.45], [0.5,0.55]]) #WITHOUT BIAS

def activation(summation):
    return 1/(1 + np.exp(-summation))

def feed_forward(sample, output_sample):
    global a_vector
    # Hidden neurons output considering bias
    a_vector = activation(np.matmul(w_matrix, np.insert(sample, 0, 1)))
    # Output neuron output
    output_array = activation(np.matmul(j_matrix, a_vector))
    #Error calculation
    e_total = 0
    for i in range(len(output_array)):
        e_total += 0.5*(output_sample[i] - output_array[i])**2

    return e_total, output_array


def generate_deltas(output_sample, actual_output):
    global m_vector
    global z_vector

    # Generate deltas for output layer (m_vector)
    for i in range(OUTPUT_QTY):
        m_vector[i] = -(output_sample[i] - actual_output[i]) * actual_output[i] * (1 - actual_output[i])
        # TODO: miss to multiply by corresponding input (output of hidden layer) when iterating over each j value to update  0.7413 0.1868

    # Generate deltas for hidden layer (z vector)
    b_vector = np.matmul(m_vector, j_matrix)
    for i in range(NEURONS_QTY):
        z_vector[i] = b_vector[i]*a_vector[i]*(1-a_vector[i])
        # TODO: miss to multiply by corresponding input (input layer) when iterating over each w value to update

def generate_gradients(sample):
    global w_matrix, m_vector, a_vector, z_vector
    sample = np.insert(sample, 0, 1)

    # Update output weights j
    for i in range(OUTPUT_QTY):
        for j in range(NEURONS_QTY):
            j_matrix[i][j] -= a_vector[j] * m_vector[i] * ETA

    # Update hidden weights w
    for i in range(NEURONS_QTY):
        for j in range(X_VECTOR_LENGTH + 1):
            w_matrix[i][j] -= z_vector[i] * sample[j] * ETA

def backpropagation(sample, output_sample, output_array):
    generate_deltas(output_sample, output_array)
    generate_gradients(sample)


if __name__ == "__main__":

    print("""
///////////////////////////////////////////////
Simple Neural Network Test

See: https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/
Tecnologias de Sistemas Inteligentes 
Professor: PhD. Benjamin Valdes Aguirre 
Moises Diaz Malagon A01208580
March 12, 2018 
///////////////////////////////////////////////""")

    # Historical records array
    w1_history = []
    w2_history = []
    w3_history = []
    w4_history = []
    w5_history = []
    w6_history = []

    j1_history = []
    j2_history = []
    j3_history = []
    j4_history = []

    output_history = []

    e_total = 10000
    iteration_num = 0
    while e_total > 0.0000001:
        for i in range(len(y_samples)):
            e_total, output_array = feed_forward(x_samples[i], y_samples[i])
            # print("////// {} : {} //////////".format(output, y_samples[i]))
            backpropagation(x_samples[i], y_samples[i], output_array)
        print("\r Error progress: {}".format(e_total), end="")

        # Make historic records
        w1_history.append(w_matrix[0][0])
        w2_history.append(w_matrix[0][1])
        w3_history.append(w_matrix[0][2])
        w4_history.append(w_matrix[1][0])
        w5_history.append(w_matrix[1][1])
        w6_history.append(w_matrix[1][2])

        j1_history.append(j_matrix[0][0])
        j2_history.append(j_matrix[0][1])
        j3_history.append(j_matrix[1][0])
        j4_history.append(j_matrix[1][1])
        iteration_num += 1


    # Plot history of hidden layer weights
    x = range(iteration_num)
    plt.figure(1)
    plt.subplot(2, 3, 1)
    plt.plot(x, w1_history)
    plt.title('W Neuron 0 input 0 (bias)')
    plt.xlabel('Iteration Num')

    plt.subplot(2, 3, 2)
    plt.plot(x, w2_history)
    plt.title('W Neuron 0 input 1')
    plt.xlabel('Iteration Num')
    
    plt.subplot(2, 3, 3)
    plt.plot(x, w3_history)
    plt.title('W Neuron 0 input 2')
    plt.xlabel('Iteration Num')

    plt.subplot(2, 3, 4)
    plt.plot(x, w4_history)
    plt.title('W Neuron 2 input 0 (bias)')
    plt.xlabel('Iteration Num')

    plt.subplot(2, 3, 5)
    plt.plot(x, w5_history)
    plt.title('W Neuron 2 input 1')
    plt.xlabel('Iteration Num')

    plt.subplot(2, 3, 6)
    plt.plot(x, w6_history)
    plt.title('W Neuron 2 input 2')
    plt.xlabel('Iteration Num')

    plt.tight_layout()
    plt.show()

    plt.figure(2)
    # Plot history of output layer weights
    plt.subplot(2, 2, 1)
    plt.plot(x, j1_history)
    plt.title('J Output Neuron 0, Neuron 0')
    plt.xlabel('Iteration Num')

    plt.subplot(2, 2, 2)
    plt.plot(x, j2_history)
    plt.title('J Output Neuron 0, Neuron 1')
    plt.xlabel('Iteration Num')


    plt.subplot(2, 2, 3)
    plt.plot(x, j3_history)
    plt.title('J Output Neuron 1, Neuron 0')
    plt.xlabel('Iteration Num')


    plt.subplot(2, 2, 4)
    plt.plot(x, j4_history)
    plt.title('J Output Neuron 1, Neuron 1')
    plt.xlabel('Iteration Num')

    plt.tight_layout()
    plt.show()

    print("\n\n///////////////////////////////////// \n \nFinal weights matrix: \n")
    print(w_matrix)
    print("\n \nOutput weights matrix: \n")
    print(z_vector)


    print("\n\n")
    for i in range(len(y_samples)):
        print(" {} : {}   vs  {}".format(x_samples[i], output_array, y_samples[i]), end="")

    # Generate evidence
    # y_tested = []
    # for i in range(len(x_samples)):
    #     _, _, output = feed_forward(x_samples[i], y_samples[i])
    #     y_tested.append(output)
    #
    # plt.plot(x_samples, y_samples, x_samples, y_tested)
    # plt.show()

    np.save("w_matrix", w_matrix)
    np.save("j_matrix", j_matrix)