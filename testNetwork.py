import numpy as np

w_matrix = np.load("w_matrix.npy")
j_vector = np.load("j_vector.npy")


x_samples = np.array([
    [1.0],
    [2.9]
])

y_samples = np.array([0.99, 0.4])



NEURONS_QTY = 3
X_VECTOR_LENGTH = len(x_samples[0])
ETA = 3.0

a_vector = np.zeros(NEURONS_QTY)

#Deltas matrix. For hidden layer z
z_vector = np.zeros(NEURONS_QTY)
#For output layer m
m_vector = np.zeros(1)

#Gradients matrix. For input weights wg
wg_vector = np.zeros(X_VECTOR_LENGTH + 1)
#Gradients for output weights
og_vector = np.zeros(NEURONS_QTY)


# w_matrix = np.array([[0.94442221, 0.60891922, 0.8681712, 0.62541323], [0.96158112,0.61785532,0.94325846,0.16133993], [0.766387,0.71727074,0.57041841,0.28283347]])
# j_vector = np.array([1,2,3])

def activation(summation):
    return 1/(1 + np.exp(-summation))

def feed_forward(sample, output_sample):
    global a_vector
    a_vector = activation(np.matmul(w_matrix, np.insert(sample, 0, 1)))
    output = activation(np.matmul(a_vector,j_vector))
    error = output_sample - output
    sqerror = (error)**2
    return sqerror, error, output



if __name__ == "__main__":
    for i in range(len(x_samples)):
        _, error, output = feed_forward(x_samples[i], y_samples[i])
        print("{} : {}  vs {}".format(x_samples[i], output, y_samples[i]))
