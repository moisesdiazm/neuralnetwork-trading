# Neural network implementation
#

import numpy as np
from matplotlib import pyplot as plt

from auxiliarFunctions import preprocessing_csv, preprocess_samples
from scaling import normalize_single_input, normalize_input


# w_matrix = np.array([[0.35, 0.15, 0.20], [0.35,0.25,0.30]]) #WITH BIAS
# j_matrix = np.array([[0.4,0.45]]) #WITHOUT BIAS

def activation(summation):
    return 1/(1 + np.exp(-summation))

def feed_forward(sample, output_sample):
    global a_vector
    # Hidden neurons output considering bias
    a_vector = activation(np.matmul(w_matrix, np.insert(sample, 0, 1)))
    # Output neuron output
    output_array = activation(np.matmul(j_matrix, a_vector))
    #Error calculation
    e_total = 0
    for i in range(len(output_array)):
        e_total += 0.5*(output_sample[i] - output_array[i])**2

    return e_total, output_array


def generate_deltas(output_sample, actual_output):
    global m_vector
    global z_vector

    # Generate deltas for output layer (m_vector)
    for i in range(OUTPUT_QTY):
        m_vector[i] = -(output_sample[i] - actual_output[i]) * actual_output[i] * (1 - actual_output[i])
        # TODO: miss to multiply by corresponding input (output of hidden layer) when iterating over each j value to update  0.7413 0.1868

    # Generate deltas for hidden layer (z vector)
    b_vector = np.matmul(m_vector, j_matrix)
    for i in range(NEURONS_QTY):
        z_vector[i] = b_vector[i]*a_vector[i]*(1-a_vector[i])
        # TODO: miss to multiply by corresponding input (input layer) when iterating over each w value to update

def generate_gradients(sample):
    global w_matrix, m_vector, a_vector, z_vector
    sample = np.insert(sample, 0, 1)

    # Update output weights j
    for i in range(OUTPUT_QTY):
        for j in range(NEURONS_QTY):
            j_matrix[i][j] -= a_vector[j] * m_vector[i] * ETA

    # Update hidden weights w
    for i in range(NEURONS_QTY):
        for j in range(X_VECTOR_LENGTH + 1):
            w_matrix[i][j] -= z_vector[i] * sample[j] * ETA

def backpropagation(sample, output_sample, output_array):
    generate_deltas(output_sample, output_array)
    generate_gradients(sample)


if __name__ == "__main__":

    print("""
///////////////////////////////////////////////
Neural Network Implementation

Tecnologias de Sistemas Inteligentes 
Professor: PhD. Benjamin Valdes Aguirre 

Moises Diaz Malagon A01208580
March 12, 2018 
    ///////////////////////////////////////////////""")

    # Data ingestion and preprocessing
    print("Getting the samples")

    preprocessing_csv()
    df = preprocess_samples()

    training_df = df[0:69]
    x_samples = training_df[['time']].as_matrix()
    y_samples = training_df[['price']].as_matrix()

    print("Normalizing samples")
    scaled_x_samples, scaling_factors = normalize_input(x_samples)
    np.save("normalization_parameters", scaling_factors)

    #Global variables initialization

    NEURONS_QTY = 6
    OUTPUT_QTY = 1
    X_VECTOR_LENGTH = len(x_samples[0])
    ETA = 1.0

    w_matrix = np.random.rand(NEURONS_QTY, X_VECTOR_LENGTH + 1)  # +1 for bias
    j_matrix = np.random.rand(OUTPUT_QTY, NEURONS_QTY)
    a_vector = np.zeros(NEURONS_QTY)  # output from hidden layer

    # Deltas matrix. For hidden layer z
    z_vector = np.zeros(NEURONS_QTY)
    # For output layer m
    m_vector = np.zeros(len(y_samples[0]))

    # Gradients matrix. For input weights wg
    wg_vector = np.zeros(X_VECTOR_LENGTH + 1)
    # Gradients for output weights
    og_vector = np.zeros(NEURONS_QTY)


    # Main algorithm starts

    err_sum = 10000
    while err_sum > 0.03:
        err_sum = 0
        for i in range(len(y_samples)):
            e_total, output_array = feed_forward(scaled_x_samples[i], y_samples[i])
            # print("////// {} : {}  vs  {}//////////".format(x_samples[i],output_array, y_samples[i]))
            err_sum += e_total
            backpropagation(scaled_x_samples[i], y_samples[i], output_array)
        print("\r Error progress: {}".format(err_sum), end="")

    print("\n\n///////////////////////////////////// \n \nFinal weights matrix: \n")
    print(w_matrix)
    print("\n \nOutput weights matrix: \n")
    print(z_vector)

    # Generate evidence on unseen input data
    x_new_samples = np.array([[x] for x in np.linspace(0, 1.0, 100)])
    y_tested = []
    for i in range(len(x_new_samples)):
        _, output_array = feed_forward(normalize_single_input(x_new_samples[i]), x_new_samples[i])
        y_tested.append(output_array)


    x_complete = df[['time']].as_matrix()
    y_complete = df[['price']].as_matrix()

    plt.plot(x_complete, y_complete, label="Real World Data")
    plt.plot(x_new_samples, y_tested, label="Forecast")
    plt.plot(x_samples, y_samples, label="Training")
    plt.legend(bbox_to_anchor=(0, 1), loc=2, borderaxespad=0.)

    plt.show()

    np.save("w_matrix", w_matrix)
    np.save("j_matrix", j_matrix)